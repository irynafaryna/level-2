// FRAGMENT SHADER

#version 330

in vec4 color;
out vec4 outColor;

uniform sampler2D texture0;
in vec2 texCoord0;

in vec4 position;
in vec3 normal;

// Materials
uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float shininess;

//shadow
in vec4 shadowCoord;
uniform sampler2DShadow shadowMap;

// View Matrix
uniform mat4 matrixView;
struct SPOT
{
    vec3 position;
    vec3 diffuse;
    vec3 specular;
    //spot
    vec3 direction;
    float cutoff;
    float attenuation;
    //animated light
    mat4 matrix;
    };
uniform SPOT lightSpot;

vec4 SpotLight(SPOT light)
{
    vec4 color = vec4(0, 0, 0, 0);
    vec3 L = normalize(light.matrix * vec4(light.position, 1) - position).xyz;
    float NdotL = dot(normal, L);
    vec3 V = normalize(-position.xyz);
    vec3 R = reflect(-L, normal);
    float RdotV = dot(R, V);
    if (NdotL > 0 && RdotV > 0)
        color += vec4(materialSpecular * light.specular * pow(RdotV, shininess), 1);
    if (NdotL > 0)
    color += vec4(materialDiffuse * light.diffuse, 1) * NdotL;

    vec3 D = normalize(mat3(light.matrix)*light.direction);
    float spotFactor = dot(-L, D);
    float a = acos(spotFactor);
    if (a <= light.cutoff) {
        spotFactor = pow(spotFactor, light.attenuation);
    } else{
        spotFactor = 0;
    }
    return spotFactor * color;
}

void main(void) 
{
        // Calculation of theshadow
    float shadow = 1.0;
    if(shadowCoord.w > 0)// if shadowCoord.w < 0 fragment is out of the Light POV
    shadow = 0.5+ 0.5* textureProj(shadowMap, shadowCoord);

  outColor = color;
  outColor += SpotLight(lightSpot);
  outColor *= texture(texture0, texCoord0);
  outColor *= shadow;
}